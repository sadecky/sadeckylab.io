const header = document.querySelector("header");
window.addEventListener("scroll", function () {
  header.classList.toggle("sticky", this.window.scrollY > 0);
});

let options = {
  startAngle: -1.55,
  size: 150,
  value: 0.85,
  fill: { gradient: ["#2a3030", "#2a3030"] },
};
$(".circle .bar")
  .circleProgress(options)
  .on("circle-animation-progress", function (event, progress, stepValue) {
    $(this)
      .parent()
      .find("span")
      .text(String(stepValue.toFixed(2).substr(2)) + "%");
  });
$(".HTML .bar").circleProgress({
  value: 0.6,
});
$(".SQL .bar").circleProgress({
  value: 0.7,
});
$(".Postman .bar").circleProgress({
  value: 0.6,
});
$(".JIRA .bar").circleProgress({
  value: 0.8,
});
$(".Python .bar").circleProgress({
  value: 0.5,
});


let menu = document.querySelector("#menu-icon");
let navlist = document.querySelector(".navlist");

menu.onclick = () => {
  menu.classList.toggle("bx-x");
  navlist.classList.toggle("open");
};

window.onscroll = () => {
  menu.classList.remove("bx-x");
  navlist.classList.remove("open");
};
